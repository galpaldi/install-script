import glob
import os, time
from stat import * # ST_SIZE etc
from datetime import date

today = date.today()
full_dt = str(today.year) + str(today.month) + str(today.day)
#today.replace("-","")
print("Today's date:", today, full_dt)


topdir = "C:/OneDrive/OneDrive - Matrix-Exzac/Projects/DataSure/Scripts"

for dirpath, dirnames, files in os.walk(topdir):
    for name in files:
        #if name.lower().endswith(exten):
        full_path = os.path.join(dirpath, name)

        try:
            st = os.stat(full_path)
        except IOError:
            print ("failed to get information about", full_path)
        else:
            print (full_path + "-->" + time.asctime(time.localtime(st[ST_MTIME])) )

print("End....")
