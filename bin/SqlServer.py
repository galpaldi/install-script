import pypyodbc as pyodbc
from cfrm import *

# Logging configuration
logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(name)s\t%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))

def SqlServer(vConfFileFullPath):
    ReadConf(vConfFileFullPath)

    vHost = dConfFile['SQLSERVER_HOST']
    vDB   = dConfFile['SQLSERVER_DB']
    vUser = dConfFile['SQLSEWRVER_USER']
    vPass = dConfFile['SQLSERVER_PWD']

    cnxn = ConnectDatabase('SQLSERVER', vUser, vPass, vHost, vDB)

    InstallOrder = (
    'sequence',
    'table',
    'index',
    'sqlserver_cons',
    'constraint')
    # 'procedure',
    # 'trigger',
    # 'insert'
    # )

    cursor = cnxn.cursor()

    vRepoPath = input("\tRepository Base Path: ")
    vInstallVersion = input("\tVersion to Install: ")

    vFullRepositorytoInstall = vRepoPath + "\\" + vInstallVersion

    if not os.path.isdir(vFullRepositorytoInstall):
        print ("Path :", vFullRepositorytoInstall, " Not Exists")
        # curMain.close()
        # con.close()
        exit(3)

    for vObjectType in InstallOrder:
        pkDDL = []
        fkDDL = []
        logger.info("\tCreating {}".format(vObjectType))

        vObjectPath = vFullRepositorytoInstall + "\\database\\scratch\\" + vObjectType
        for dirpath, dirnames, files in os.walk(vObjectPath):
            for name in files:
                #if name.lower().endswith(exten):
                full_path = os.path.join(dirpath, name)
                fTable = open(full_path, 'r')
                vTableDDL =fTable.read()
                fTable.close()

                if vObjectType == "sqlserver_cons":
                    vTableDDL = re.sub('database_name', vDB, vTableDDL)

                if vObjectType == "sequence":
                    vTableDDL = re.sub('CREATE SEQUENCE  ', 'CREATE SEQUENCE dbo.', vTableDDL)
                    vTableDDL = re.sub(r'CACHE|NOCACHE', '', vTableDDL)
                    vTableDDL = re.sub(r'ORDER|NOORDER', '', vTableDDL)
                    vTableDDL = re.sub(r'CYCLE|NOCYCLE', '', vTableDDL)
                    vTableDDL = re.sub(r'KEEP|NOKEEP', '', vTableDDL)
                    vTableDDL = re.sub(r'SCALE|NOSCALE', '', vTableDDL)
                    vTableDDL = re.sub('GLOBAL', '', vTableDDL)
                    vTableDDL = re.sub(r'999999.*', '9223372036854775807', vTableDDL)

                if vObjectType == "table":
                    vTableDDL = re.sub('CREATE TABLE ', 'CREATE TABLE ' + vDB + '.dbo.', vTableDDL)

                    vTableDDL = re.sub('NUMBER','BIGINT', vTableDDL)
                    vTableDDL = re.sub('VARCHAR2','VARCHAR', vTableDDL)
                    vTableDDL = re.sub('DATE','DATETIME', vTableDDL)
                    vTableDDL = re.sub('CLOB','VARCHAR(MAX)', vTableDDL)

                    vTableDDL = re.sub('DEFAULT SEQ_PRIMARY_ID.NEXTVAL',' constraint df_id@' + name + '_sq default next value for dbo.SEQ_PRIMARY_ID', vTableDDL)
                    vTableDDL = re.sub("DEFAULT sysdate", 'constraint df_start_date@' + name + ' DEFAULT GETDATE()', vTableDDL)
                    vTableDDL = re.sub("DEFAULT to_date\('12/31/2999', 'mm/dd/yyyy'\)", "constraint df_end_date@" + name + " DEFAULT CONVERT(DATETIME, '2999-12-31', 102)", vTableDDL)
                    vTableDDL = re.sub("DEFAULT SYS_CONTEXT\('USERENV','OS_USER'\)\|\|'@'\|\|SYS_CONTEXT\('USERENV','HOST'\)", "constraint df_creator@" + name + " DEFAULT suser_name() + '@' + HOST_NAME()", vTableDDL)

                if vObjectType == "index" :
                    vTableDDL = re.sub(' ON ', ' ON ' + vDB +'.dbo.', vTableDDL)

                    if re.search(r'_PK ', vTableDDL):
                        continue

                if vObjectType == "constraint":
                    if re.search(r'PRIMARY KEY', vTableDDL):
                        pkDDL.append(vTableDDL + "|" + name)
                    else:
                        fkDDL.append(vTableDDL + "|" + name)
                else:
                    try:
                        cursor.execute(vTableDDL)
                        cnxn.commit()
                    except pyodbc.Error as e:
                        logger.error("\t{}".format(e))
                        #exit(3)

            for pkConstDDL in pkDDL:
                (vPKDDL, name) = pkConstDDL.split("|")
                print ("\tCreate ...", name)

                vPKDDL = re.sub(' USING INDEX  ENABLE','', vPKDDL)
                try:
                    cursor.execute(vPKDDL.strip())
                    cnxn.commit()
                except pyodbc.Error as e:
                    logger.error("\t{}".format(e))
                    #exit(3)

            for fkConstDDL in fkDDL:
                (vFKDDL, name) = fkConstDDL.split("|")
                print ("\tCreate ...", name)

                vFKDDL = re.sub(' ENABLE','', vFKDDL)
                try:
                    cursor.execute(vFKDDL.strip())
                    cnxn.commit()
                except pyodbc.Error as e:
                    logger.error("\t{}".format(e))
                    #exit(3)

    cnxn.close()
