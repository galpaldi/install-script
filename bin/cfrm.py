import os
import re
import cx_Oracle
import pypyodbc as pyodbc
import sys
import getopt
from os import path
import logging

# Logging configuration
logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(name)s\t%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))

dConfFile = {}

def ReadConf(pConfFilePath):
    vResult = 0

    if os.path.isfile(pConfFilePath):
        fConfFile = open(pConfFilePath, 'r')
        vConfFileContent = fConfFile.readlines()

        for line in vConfFileContent:
            if re.match("^[#|/\n]", line):
                continue
            else:
                vParameter, vValue = line.split("=")
                dConfFile[vParameter.strip().upper()] = vValue.strip()
    else:
        print ("Canot find configuration file: ", pConfFilePath)
        vResult = 1


    return vResult

def ConnectDatabase(pDB, pUser, pPassword, pHostDetails, pSQLDB):
    con = None

    if pDB.upper() == 'ORACLE':
        vConnection = pUser + "/" + pPassword + "@" + pHostDetails

        try:
            con = cx_Oracle.connect(vConnection)
            #con = cx_Oracle.connect(pUser, pPassword, pHostDetails)
        except cx_Oracle.DatabaseError as e:
            logger.error("\t{}".format(e))
            exit(3)

    elif pDB.upper() == 'SQLSERVER':
        try:
            con = pyodbc.connect('DRIVER={SQL Server};SERVER='+pHostDetails+';DATABASE='+pSQLDB+';UID='+pUser+';PWD='+ pPassword)
        except pyodbc.Error as e:
            logger.error("\t{}".format(e))
            exit(3)

    else:
        logger.error("Database: {} is not supported.".format(pDB) )

    return con
