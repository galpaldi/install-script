import platform
import subprocess
import sys
from cfrm import *

# Logging configuration
logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(name)s\t%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))



def CreRepo(vConfFileFullPath):
    OSPlatform = platform.platform().split('-')[0].upper()

    ReadConf(vConfFileFullPath)

    vMainRepo = dConfFile['MAIN_REPO_DIR']

    vUser = dConfFile['REPO_USER']
    vPass = dConfFile['REPO_PASS']
    vHost = dConfFile['REPO_HOST']

    con = ConnectDatabase('ORACLE', vUser, vPass, vHost,'')

    curMain = con.cursor()
    curLoop = con.cursor()
    curObj  = con.cursor()

    sqlToExec = """
select datasure_version
from datasure.t_datasure_version
where end_date = to_date('12/31/2999', 'mm/dd/yyyy')
"""

    curMain.execute(sqlToExec)
    vDatasureVersion = ''
    for result in curMain:
        vDatasureVersion = result[0]

    if vDatasureVersion == '':
        logger.error("Datasure Version is not set (Table: datasure.t_datasure_version)")
        exit(1)
    else:
        #print ("Datasure Version = ", vDatasureVersion)
        logger.info("Datasure Version = {}".format(vDatasureVersion))
        vVersionRepo = vMainRepo + "\\" + vDatasureVersion

    if not os.path.isdir(vVersionRepo):
        logger.info("Datasure Version dir {} Not exists... Creating".format(vDatasureVersion))

        if OSPlatform == 'WINDOWS':
            mkdirCMD = "mkdir " + vVersionRepo
        else:
            mkdirCMD = "mkdir -p " + vVersionRepo

        returned_value = subprocess.call(mkdirCMD, shell=True)

    vDatabasePathScratch = vVersionRepo + "\\database\\scratch"

    sqlToExec = """
select object_type, count(*)
from dba_objects
where owner = 'DATASURE'
and object_type not in ('LOB')
group by object_type
union all
select 'CONSTRAINT', count(*)
from dba_constraints
where owner = 'DATASURE'
and constraint_type in ('P','R')
union all
select 'INSERT', 0 from dual
union all
select 'SQLSERVER_CONS', 0 from dual
    """
    curMain.execute(sqlToExec)
    for result in curMain:
        vObjectType  = result[0]
        vObjectCount = result[1]

        if OSPlatform == 'WINDOWS':
            vObjectPath = vDatabasePathScratch + "\\" + vObjectType.lower()
            mkdirCMD = "mkdir " + vObjectPath
        else:
            vObjectPath = vDatabasePathScratch + "/" + vObjectType.lower()
            mkdirCMD = "mkdir -p " + vObjectPath


        if not os.path.isdir(vObjectPath):
            returned_value = subprocess.call(mkdirCMD, shell=True)

        logger.info("\tCreating {}".format(vObjectType))
        if vObjectType == 'SQLSERVER_CONS':
            sqlObjExec = """
select 'alter table [database_name].[dbo].'||table_name||' alter column '||column_name||' '||data_type||' not null;'
from (
    select  cc.*,
            case
                when t.data_type = 'VARCHAR2' then 'varchar('||data_length||')'
                when t.data_type = 'NUMBER'   then 'bigint'
                when data_type   = 'DATE'     then 'datetime'
                else 'ERROR!!'
            end as data_type
    from dba_cons_columns cc inner join dba_constraints c
    on (c.constraint_name = cc.constraint_name)
    inner join dba_tab_columns t
    on (t.table_name = cc.table_name and
        t.column_name = cc.column_name
    ) where c.constraint_type = 'P'
    and c.owner = 'DATASURE'
    and c.constraint_name not like 'BIN$%'
)
"""
            if OSPlatform == 'WINDOWS':
                vFileName = vDatabasePathScratch + "\\" + vObjectType.lower() + "\\alter_table_not_null"
            else:
                vFileName = vDatabasePathScratch + "/" + vObjectType.lower() + "/alter_table_not_null"

            try:
                fObjectFile = open(vFileName, 'w')
            except:
                logger.error("Cannot create file: {} ".format(vFileName))
                con.close()
                exit(2)

            curObj.execute(sqlObjExec)
            for alterTables in curObj:
                fObjectFile.write(alterTables[0].lower()+"\n")

            fObjectFile.close()
            continue


        if vObjectType == 'INSERT':
            sqlObjExec = """
insert into t_datasure_version (datasure_version) values ('""" + vDatasureVersion + """')
"""
            if OSPlatform == 'WINDOWS':
                vFileName = vDatabasePathScratch + "\\" + vObjectType.lower() + "\\insert_base_data"
            else:
                vFileName = vDatabasePathScratch + "/" + vObjectType.lower() + "/insert_base_data"

            try:
                fObjectFile = open(vFileName, 'w')
            except:
                logger.error("Cannot create file: {} ".format(vFileName))
                con.close()
                exit(2)

            fObjectFile.write(sqlObjExec)
            fObjectFile.close()
            continue

        if vObjectType == 'CONSTRAINT':
            sqlObjExec = """
    select constraint_name
    from dba_constraints
    where owner = 'DATASURE'
    and constraint_type in ('P', 'R')
    and constraint_name not like 'BIN$%'
    """
        else:
            sqlObjExec = """
    select object_name
    from dba_objects
    where owner = 'DATASURE'
    and object_name not like 'SYS_IL%'
    and object_name not like 'BIN$%'
    and object_type = '""" + vObjectType + """'
    """

        curObj.execute(sqlObjExec)
        for vObjectNames in curObj:
            vObjectName = vObjectNames[0].lower()
            #print ("Handeling --> ", vObjectName)

            if OSPlatform == 'WINDOWS':
                vFileName = vDatabasePathScratch + "\\" + vObjectType.lower() + "\\" + vObjectName.lower()
            else:
                vFileName = vDatabasePathScratch + "/" + vObjectType.lower() + "/" + vObjectName.lower()

            try:
                fObjectFile = open(vFileName, 'w')
            except:
                logger.error("Cannot create file: {} ".format(vFileName))
                con.close()
                exit(2)


            sqlLoopExec = """
BEGIN
    DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'SQLTERMINATOR', false);
    DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'PRETTY', true);
    DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'SEGMENT_ATTRIBUTES', false);
    DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'STORAGE', false);
    DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'CONSTRAINTS' ,false);
    DBMS_METADATA.set_transform_param (DBMS_METADATA.session_transform, 'REF_CONSTRAINTS' ,false);
END;
"""
            curLoop.execute(sqlLoopExec)
            if vObjectType == 'CONSTRAINT':
                sqlLoopExec = """
    select dbms_metadata.get_ddl (case
                                    when constraint_type = 'R' then 'REF_CONSTRAINT'
                                    when constraint_type = 'P' then 'CONSTRAINT'
                                  end, constraint_name, owner)
    from dba_constraints
    where owner = 'DATASURE'
    and constraint_name not like 'BIN$%'
    and constraint_name = upper('""" + vObjectName + """')
    """
            else:
                sqlLoopExec = """
    select dbms_metadata.get_ddl('""" + vObjectType + """', object_name, owner)
    from dba_objects
    where owner = 'DATASURE'
    and object_name not like 'BIN$%'
    and object_type = '""" + vObjectType + """'
    and object_name = upper('""" + vObjectName + """')
    """

            curLoop.execute(sqlLoopExec)
            for result2 in curLoop:
                vDDLStmt = result2[0].read().strip().replace('"', "").replace("DATASURE.", "").replace("ALTER TRIGGER", "-- ALTER TRIGGER")
                fObjectFile.write(vDDLStmt)

            fObjectFile.close()

    con.close()
