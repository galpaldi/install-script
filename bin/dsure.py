#!/usr/bin/env python

################################################################################
# Script Name:
# Description:
# Date:         Author:
# Version:
#
#
# Date      Change by       Comment
# -----     ----------      -------------
#
################################################################################
from crerepo import *
from SqlServer import *
from oracle import *
from cfrm import *

# Logging configuration -
logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(name)s\t%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))

def usage():
    print("Usage: {} <Configuration file full path> -h , -o , -d, ".format(os.path.basename(__file__)))
    print("-h\tprint this usage")
    print("-o\tOperation:[crerep|install] This parameter require.")
    print("-d\tDatabase type[oracle|sqlserver] This parameter require.")
    exit(2)

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"ho:d:c:",[])
    except getopt.GetoptError:
        print ("GetoptError:")
        usage()

    for opt, arg in opts:
        if opt == '-o':
            vOperation = arg;
        elif opt == '-d':
            vDatabase = arg
        elif opt == '-h':
            usage()


    if vOperation.lower() == "crerepo":
        logger.info("Starting operation={}".format(vOperation.lower()))
        CreRepo(vConfFileFullPath)
        logger.info("Successful... {}".format(vOperation.lower()))
    elif vOperation.lower() == "install" and vDatabase.lower() == 'oracle':
        logger.info("Starting operation={} database={}".format(vOperation.lower(), vDatabase.lower()))
        oracle(vConfFileFullPath)
        logger.info("Successful... {}".format(vOperation))
    elif vOperation.lower() == "install" and vDatabase.lower() == 'sqlserver':
        logger.info("Starting operation={} database={}".format(vOperation.lower(), vDatabase.lower()))
        SqlServer(vConfFileFullPath)
        logger.info("Successful... {}".format(vOperation))
    else:
        print ("Nothing to do ....")



# Main
vConfFileFullPath = ''

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print ("MainError:")
        usage()

    vConfFileFullPath = sys.argv[1]

    if path.exists(vConfFileFullPath):
        logger.info("Configuration file is valid.")
        main(sys.argv[2:])
    else:
        print ("ConfError:")
        usage()
