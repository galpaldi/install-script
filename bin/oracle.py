from getpass import getpass
from cfrm import *

# Logging configuration
logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(name)s\t%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))

def oracle(vConfFileFullPath):
    ReadConf(vConfFileFullPath)

    vHost = dConfFile['ORACLE_HOST']
    vUser = dConfFile['ORACLE_USER']
    vPass = dConfFile['ORACLE_PWD']

    con = ConnectDatabase('ORACLE', vUser, vPass, vHost, '')

    InstallOrder = (
    'sequence',
    'table',
    'index',
    'constraint',
    'procedure',
    'trigger',
    'insert'
    )

    logger.info("Install Datasure on new Oracle schema:")
    curMain = con.cursor()

    vSQLStmt = """
select count(*)
from user_objects
"""
    curMain.execute(vSQLStmt)
    for result in curMain:
        vObjectCount = int(result[0])

    if vObjectCount > 0:
        logger.error("Schema {} Contains [{}] objects - Cannot install.".format(vUser, vObjectCount) )
        curMain.close()
        con.close()
        exit(4)


    vRepoPath = input("\tRepository Base Path: ")
    vInstallVersion = input("\tVersion to Install: ")

    vFullRepositorytoInstall = vRepoPath + "\\" + vInstallVersion

    if not os.path.isdir(vFullRepositorytoInstall):
        print ("Path :", vFullRepositorytoInstall, " Not Exists")
        curMain.close()
        con.close()
        exit(3)


    for vObjectType in InstallOrder:
        pkDDL = []
        fkDDL = []
        print ("Installing ", vObjectType)
        vObjectPath = vFullRepositorytoInstall + "\\database\\scratch\\" + vObjectType
        for dirpath, dirnames, files in os.walk(vObjectPath):
            for name in files:
                #if name.lower().endswith(exten):
                full_path = os.path.join(dirpath, name)
                fTable = open(full_path, 'r')
                vTableDDL =fTable.read()
                fTable.close()

                if vObjectType == "constraint":
                    if re.search(r'PRIMARY KEY', vTableDDL):
                        pkDDL.append(vTableDDL + "|" + name)
                    else:
                        fkDDL.append(vTableDDL + "|" + name)
                else:
                    print ("\tCreate ...", name)
                    curMain.execute(vTableDDL)

        if vObjectType == "constraint":
            for pkConstDDL in pkDDL:
                (vPKDDL, name) = pkConstDDL.split("|")
                print ("\tCreate ...", name)
                curMain.execute(vPKDDL.strip())

            for fkConstDDL in fkDDL:
                (vFKDDL, name) = fkConstDDL.split("|")
                print ("\tCreate ...", name)
                curMain.execute(vFKDDL.strip())

    curMain.close()
    con.commit()
    con.close()
