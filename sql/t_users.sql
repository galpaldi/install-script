CREATE TABLE t_users (
  id number default SEQ_PRIMARY_ID.nextval unique,
  user_id number  ,
  username varchar2(50),
  firstName varchar2(50),
  lastName varchar2(50),
  email varchar2(50),
  password varchar2(50),
  isAdmin number,
  creator varchar2(50) default SYS_CONTEXT('USERENV','OS_USER')||'@'||SYS_CONTEXT('USERENV','HOST') ,
  createdDate Date default sysdate,
  updatedDate Date  default sysdate
);
